This command line tool displays movies sorted by average rating, then sorted by year, then by title for every genre.  
There are options to tune the script to display:  

+ desired number of movies  
+ movies of specified genres  
+ movies for specific period  
+ to make search through titles  

There are following arguments that could be specified during launch from a command line:  

- `-n` or `--n` How many top movies to display for each genre  
- `-g` or `--genres` Movies of what genre/genres to display You can specify:    
    - one genre: `-g Comedy `
    - few genres: `-g "Comedy|Adventure"` displays top N movies for genre Comedy and top N movies for genre Adventure
    - combined genres: `-g "Comedy&Adventure"` displays top N movies which are belong to genres Comedy and Adventure

- `-yf` or `--year_from` Select movies starting from the specific year  
- `-yt` or `--year_to` Select movies up to (including) specified year  
- `-r` or `--regexp` Select movies where regexp fits movie title, not case sensitive.  

All arguments are optional.

# Usage examples:

Display top 10 movies for ALL genres:

`top_movs.py -n 10`

All movies from 2010 to 2016 of genres Comedy and Adventure:

`top_movs.py -yf 2010 -yt 2016 -g "Comedy&Adventure" `

All movies where title contains word "toy":

`top_movs.py -r toy `

Top 5 movies of genres Film-Noir or IMAX from 2015:

`top_movs.py -yf 2015 -g "film-noir|imax" -n 5`